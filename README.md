## Java 11 Training 
This repo has been created to commit all my related lab practice related to my Java 11 certification.


## Prerequisite for Training chapter 13 (as from chapter13)
Create the following folder `/home/michael/labs`

1. data
    product101.csv:
    ```csv
    D, 101, Tea, 1.99, 0, 2019-09-19
    ```
    
    product102.csv:
    ```csv
    F, 102, Cake, 3.99, 0, 2019-09-19
    ```
   
    reviews101.csv:
    ```csv
    4, Nice hot cup of tea
    3, Rather weak tea
    4, Weak Tea
    4, Good Tea
    5, Perfect Tea
    3, Just add some lemon
    ```
   
2. reports
3. temp


## Run program as a jar (non modular version)

```console
java -jar ./out/artifacts/productmanagement_jar/productmanagement.jar
```



